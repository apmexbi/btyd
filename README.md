# Buy Till You die model for Churn predictions


#
#
#

[![APMEX](https://apmex.exceda.com/content/images/content/logo-no-headline.png)](https://apmex.exceda.com/content/images/content/logo-white.png)

#
#

# Summary

  - Buy till you die (BTYD) model is used for non contractual setting where identifying a customer as churned or not is quite complicated relative to contractual setting
  - BTYD assumes that customer has 2 choices, either to buy or to not buy. Rather than assigning specifics, BTYD gives a distribution of their buying probability.
  
# Resources

- This [coursera course](https://www.coursera.org/lecture/wharton-customer-analytics/course-introduction-and-overview-O52Gh) is taught by one of the contributors of BTYD.
- It is available for free (select audit this course while enrolling)
- The cran [vignette](https://cran.r-project.org/web/packages/BTYDplus/vignettes/BTYDplus-HowTo.pdf) has all the source code and fair explaination of the same
- A working model is placed under "\\APXSTOR01\Operations\Optimization\Nikhil Bhatewara\Churn Analysis\FirstOrder2017"  
# File Structure

  - model.R : contains code for predictions
  - results.R: contains code for validation and getting the confusion matrix
  - cm.R: contains code for confusion matrix
  - pnbd.R: original pnbd functions have a limit of only 100 transactions. This is a bug fix and must be used if pnbd model needs to be run
  - package_check.R: install and load all the necessary package
 
# Data Requirement
- Only transactional data with customer id, order data and order total

# Methodology
- BTYD plus give 8 different algorithms for predictions. I ran 3 models with all algorithm and different data sets. [Resuts attached in [TFS](http://work.apmex.com/APMEX/BI/_workitems/edit/18683)]
- Everytime pareto ggg model has the best results and hence this code contains implementation of pareto ggg
